/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.robotproject;

/**
 *
 * @author ADMIN
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char LastDirection=' ';
    
    public Robot (int x,int y,int bx,int by,int N){
        this.x =x ;
        this.y =y ;
        this.bx =bx ;
        this.by =by ;
        this.N =N ;
    //N,S,E,W
    //y-1,y+1,x+1,x-1
}
    public boolean inMap(int x,int y){
        if (x >= N || x < 0 || y >= N || y < 0){
            return false;
        }
        return true;
    }
    public boolean Walk(char direction){
        switch(direction){
            case 'N':
                if (!inMap(x,y-1)){
                    printUnableMove();
                    return false;
                }
                y = y - 1;
                break;
            case 'S':
                if (!inMap(x,y+1)){
                    printUnableMove();
                    return false;
                }
                y = y + 1;
                break;
            case 'E':
                if (!inMap(x+1,y)){
                    printUnableMove();
                    return false;
                }
                x = x + 1;
                break;
            case 'W':
                if (!inMap(x-1,y)){
                    printUnableMove();
                    return false;
                }
                x = x - 1;
                break;
        }
        LastDirection = direction;
        if(isBomb()){
            printIsBomb();
        }
        return true;
    }
     public boolean Walk(char direction, int step){
         for(int i=0; i<step;i++){
             if(!this.Walk(direction)){
                 return false;
             }
         }
         return true;
     }
     public boolean Walk(){
         return this.Walk(LastDirection);
         
     } 
      public boolean Walk(int step){
         return this.Walk(LastDirection, step);
         
     } 
    public String toString(){
        return "Robot (" + this.x + ", "+ this.y +") ";
    } 
    public void printUnableMove(){
        System.out.println("I can't move!!");
    }
    public boolean isBomb(){
        if(x == bx && y == by){
            return true;
        }
        return false;
    }
    public void printIsBomb(){
        System.out.println("Bomb found!!");
    }
}
